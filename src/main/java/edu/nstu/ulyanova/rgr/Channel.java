package edu.nstu.ulyanova.rgr;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Channel {
    private List<Order> queuedOrders = new LinkedList<>();
    private int incomingChannelTime = 0;
    private int processChannelTime = 0;
    private int processChannelEmptyTime = 0;
    private int queueSize = 0;
    private int queueTime = 0;
    private int totalQueueSize = 0;
    private int totalAwaitTime = 0;
    private int totalProcessTime = 0;

    public void process(Order order, int incomingChannelTime) {
        this.incomingChannelTime = incomingChannelTime;
        processQueue();
        if (this.incomingChannelTime > processChannelTime) {
            processChannelEmptyTime += this.incomingChannelTime - processChannelTime;
            processChannelTime = this.incomingChannelTime + order.getProcessTime();
        } else if (this.incomingChannelTime < processChannelTime) {
            processChannelTime += order.getProcessTime();
        }
        queueSize++;
        totalQueueSize += queueSize;
        order.setProcessEnd(processChannelTime);
        totalProcessTime += order.getProcessTime();
        queueTime += order.getProcessEnd() - order.getReceiveTime() - order.getProcessTime();
        queuedOrders.add(order);
    }

    public void processQueue() {
        for (Order queuedOrder : queuedOrders) {
            if (queuedOrder.getProcessEnd() < incomingChannelTime) {
                queueSize--;
            }
        }
        queuedOrders = queuedOrders.stream()
                .filter(ord -> ord.getProcessEnd() > incomingChannelTime)
                .collect(Collectors.toList());
    }

    public int getIncomingChannelTime() {
        return incomingChannelTime;
    }

    public void setIncomingChannelTime(int incomingChannelTime) {
        this.incomingChannelTime = incomingChannelTime;
    }

    public int getProcessChannelTime() {
        return processChannelTime;
    }

    public void setProcessChannelTime(int processChannelTime) {
        this.processChannelTime = processChannelTime;
    }

    public int getProcessChannelEmptyTime() {
        return processChannelEmptyTime;
    }

    public void setProcessChannelEmptyTime(int processChannelEmptyTime) {
        this.processChannelEmptyTime = processChannelEmptyTime;
    }

    public int getQueueSize() {
        return queueSize;
    }

    public void setQueueSize(int queueSize) {
        this.queueSize = queueSize;
    }

    public int getQueueTime() {
        return queueTime;
    }

    public void setQueueTime(int queueTime) {
        this.queueTime = queueTime;
    }

    public int getTotalQueueSize() {
        return totalQueueSize;
    }

    public void setTotalQueueSize(int totalQueueSize) {
        this.totalQueueSize = totalQueueSize;
    }

    public int getTotalAwaitTime() {
        return totalAwaitTime;
    }

    public void setTotalAwaitTime(int totalAwaitTime) {
        this.totalAwaitTime = totalAwaitTime;
    }

    public int getTotalProcessTime() {
        return totalProcessTime;
    }

    public void setTotalProcessTime(int totalProcessTime) {
        this.totalProcessTime = totalProcessTime;
    }
}
