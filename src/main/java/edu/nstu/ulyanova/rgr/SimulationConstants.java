package edu.nstu.ulyanova.rgr;

public final class SimulationConstants {
    private SimulationConstants() {
    }

    //Количество каналов обслуживания
    //public static final int CHANNELS_AMOUNT = 2;

    // Минимальный интервал между поступлением заявок (постановкой в очередь)
    public static final int MIN_ORDER_INTERVAL_SECONDS = 3;

    // Минимальное время обслуживания заявки (обслуживание клиента банкоматом)
    public static final int MIN_ORDER_PROCESS_SECONDS = 10;

    //Средний интервал между поступлением заявок (мат.ожидание)
    //public static final double AVERAGE_ORDER_INTERVAL_SECONDS = 60.0;

    //Среднее время обслуживания заявок (мат.ожидание)
    public static final double AVERAGE_ORDER_PROCESS_SECONDS = 70.0;

    //Обратная величина к мат.ожиданию для заявок
    //public static final double ORDER_LAMBDA_FACTOR = 1 / AVERAGE_ORDER_INTERVAL_SECONDS;

    //Обратная величина к мат.ожиданию для обработки заявок
    public static final double PROCESS_LAMBDA_FACTOR = 1 / AVERAGE_ORDER_PROCESS_SECONDS;


    //Доверительная вероятностьпо умолчанию 1 - Level of significance (α  = 0.05)
    public static final double CONFIDENCE_COEFFICIENT = 1 - 0.05;

    //Квантиль нормального распределения, определяемый по заданной доверительной вероятности = 0,95
    public static final double T_ALPHA_OF_NORMAL_DISTRIBUTION = 1.645;

    //Стартовое значение N реализаций моделировния (прогонов)
    public static final int INITIAL_MODEL_RUNS_AMOUNT = 100;
}
