package edu.nstu.ulyanova.rgr;

public class OrderProducer {
    private OrderHandler handler;

    public OrderProducer(int channelAmount) {
        handler = new OrderHandler(channelAmount);
    }

    public OrderHandler getHandler() {
        return handler;
    }
}
