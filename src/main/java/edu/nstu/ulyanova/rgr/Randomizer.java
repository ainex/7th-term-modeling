package edu.nstu.ulyanova.rgr;

import java.util.Random;

import static edu.nstu.ulyanova.rgr.SimulationConstants.*;

public class Randomizer {
    private Random random = new Random();

    private final int channelsAmount;
    private final double averageOrderInterval;
    private final double orderLambdaFactor;
    private final double processDenialProbability;
    private static Params parameters;


    public static void setUp(Params params) {
        parameters = params;
    }

    public static Randomizer getInstance() {
        return new Randomizer(parameters.channelsAmount, parameters.averageOrderIntervalSeconds, parameters.channelDenialProbability);
    }

    public Randomizer(int channelsAmount, double averageOrderInterval, double processDenialProbability) {
        this.channelsAmount = channelsAmount;
        this.averageOrderInterval = averageOrderInterval;
        this.orderLambdaFactor = 1.0 / this.averageOrderInterval;
        this.processDenialProbability = processDenialProbability;
    }

    /**
     * Генерирует случайный интервал между поступлением заявок.
     * Величина фильтруется так, чтобы быть больше минимального порогового значения
     * {@link SimulationConstants#MIN_ORDER_INTERVAL_SECONDS}
     *
     * @return случайное количество секунд
     */
    public int nextOrderInterval() {
        int result;
        do {
            result = (int) Math.round(getNextExponential(orderLambdaFactor));
        } while (result < MIN_ORDER_INTERVAL_SECONDS);
        return result;
    }

    /**
     * Генерирует случайный интервал времени обработки заявки.
     * Величина фильтруется так, чтобы быть больше минимального порогового значения
     * {@link SimulationConstants#MIN_ORDER_PROCESS_SECONDS}
     *
     * @return случайное количество секунд
     */
    public int nextProcessInterval() {
        int result;
        do {
            result = (int) Math.round(getNextExponential(PROCESS_LAMBDA_FACTOR));
        } while (result < MIN_ORDER_PROCESS_SECONDS);
        return result;
    }

    /**
     * Случайный номер канала, начиная с 0
     *
     * @return
     */
    public int nextChannelNumber() {
        return random.nextInt(channelsAmount);
    }

    /**
     * Случайный признак вероятности отказа в облсуживании
     *
     * @return признак отказа да/нет
     */
    public boolean isProcessDenial() {
        return processDenialProbability > random.nextDouble();
    }

    /**
     * Методом обратной функции генерирует СВ на основе базовой СВ, распределенной равномерно.
     *
     * @param lambdaFactor коэффициент лямбда
     * @return СВ
     */
    private double getNextExponential(double lambdaFactor) {
        return Math.log(1 - random.nextDouble()) / (-1 * lambdaFactor);
    }
}



