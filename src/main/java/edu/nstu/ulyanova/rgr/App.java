package edu.nstu.ulyanova.rgr;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Точка входа в приложение
 */
public class App {
    public static void main(String[] args) {

        //Взять проперти из конфига
        Simulator simulator = new Simulator(getParams());
        simulator.run();

    }

    private static Params getParams() {
        Properties properties = loadProperties();
        Params params = new Params();
        //задаем параметры моделирования из свойств
        params.simulationTime = Integer.parseInt(properties.getProperty("simulation.time"));
        params.channelsAmount = Integer.parseInt(properties.getProperty("channels.amount"));
        params.channelDenialProbability = Double.parseDouble(properties.getProperty("channel.denial.probability"));
        params.averageOrderIntervalSeconds = Double.parseDouble(properties.getProperty("average.order.interval.seconds"));
        params.keyIndicatorEpsilon = Double.parseDouble(properties.getProperty("key.indicator.epsilon"));

        params.fixedRuns = Boolean.parseBoolean(properties.getProperty("fixed.runs"));
        params.fixedRunsAmount = Integer.parseInt(properties.getProperty("fixed.runs.amount"));
        return params;
    }

    private static Properties loadProperties() {
        Properties properties = new Properties();

        try (InputStream input = new FileInputStream("default.properties")) {
            properties.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return properties;
    }

}
