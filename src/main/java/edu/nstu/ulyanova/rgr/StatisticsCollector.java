package edu.nstu.ulyanova.rgr;


import java.math.BigDecimal;
import java.math.RoundingMode;

public final class StatisticsCollector {
    private static final int ROUNDING_SCALE_SIZE = 2;

    private final int channelsAmount;

    public StatisticsCollector(int channelsAmount) {
        this.channelsAmount = channelsAmount;
    }

    // Общее время ожидания в очереди
    // (от поступления до начала обслуживания заявки)
    private int totalQueueTime;

    //Общее время обслуживания заявки
    private int totalProcessTime;

    //Сумма всех длин очередей
    private int totalQueueLength;

    //Общее количество поступивших заявок
    private int totalOrdersAmount;

    //Количество обслуженных заявок.
    private int processedOrdersAmount;

    public void setTotalQueueTime(int totalQueueTime) {
        this.totalQueueTime = totalQueueTime;
    }

    public void setTotalProcessTime(int totalProcessTime) {
        this.totalProcessTime = totalProcessTime;
    }

    public void setTotalQueueLength(int totalQueueLength) {
        this.totalQueueLength = totalQueueLength;
    }

    public void setTotalOrdersAmount(int totalOrdersAmount) {
        this.totalOrdersAmount = totalOrdersAmount;
    }

    public void setProcessedOrdersAmount(int processedOrdersAmount) {
        this.processedOrdersAmount = processedOrdersAmount;
    }

    /*  Показатели эффективности функционирования объекта     */

    /**
     * @return среднее время простоя заявки в очереди
     */
    BigDecimal getAverageQueueTime() {
        return round(1.0 * totalQueueTime / processedOrdersAmount);
    }

    /**
     * @return среднее время обслуживания заявки
     */
    BigDecimal getAverageProcessTime() {
        return round(1.0 * totalProcessTime / processedOrdersAmount);
    }

    /**
     * @return средняя длина очереди
     */
    BigDecimal getAverageQueueLength() {
        return round(1.0 * totalQueueLength / processedOrdersAmount);
    }

    /**
     * @return количество обслуженных заявок за время моделирования
     */
    public int getProcessedOrdersAmount() {
        return processedOrdersAmount;
    }

    /**
     * Сумма обслуживания всех зявок по всем каналам деленная на количество каналов
     *
     * @return среднее время работы канала обслуживания
     */
    BigDecimal getAverageChannelProcessTime() {
        return round(1.0 * totalProcessTime / channelsAmount);
    }

    /**
     * @return вероятность обслуживания заявки
     */
    BigDecimal getProcessedProbability() {
        return round(1.0 * processedOrdersAmount / totalOrdersAmount);
    }


    /**
     * Добалвяет параметры заявки к общей статистике
     *
     * @param order              заявка
     * @param channelQueueLength длина очереди канала в момент поступления заявки
     */
    void addStatistic(Order order, int channelQueueLength) {
        totalQueueTime += order.getProcessStart() - order.getReceiveTime();
        totalProcessTime += order.getProcessEnd() - order.getProcessStart();

        totalQueueLength += channelQueueLength;
        totalOrdersAmount += 1;

        //todo тут внимательно проверить, отказные это не обработанные?
        if (order.isProcessed()) {
            processedOrdersAmount += 1;
        }
    }

    private BigDecimal round(double value) {
        return BigDecimal.valueOf(value).setScale(ROUNDING_SCALE_SIZE, RoundingMode.HALF_UP);
    }

}