package edu.nstu.ulyanova.rgr;

public class Params {
    int simulationTime;
    int channelsAmount;
    double channelDenialProbability;
    double averageOrderIntervalSeconds;
    double keyIndicatorEpsilon;

    boolean fixedRuns;
    int fixedRunsAmount;
}
