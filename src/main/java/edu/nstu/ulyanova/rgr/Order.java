package edu.nstu.ulyanova.rgr;


/**
 * Заявка для обслуживания в канале.
 * Некоторые параменты получает от канала. Например, processStart время начала обслуживания - это время освобождения канала
 */
public class Order {
    private boolean prioritized;
    private int receiveTime;
    private int awaitStart;
    private int awaitEnd;
    private int processStart;
    private int processEnd;
    private int processTime;
    private boolean processed;

    public Order() {

    }

    public boolean isPrioritized() {
        return prioritized;
    }

    public void setPrioritized(boolean prioritized) {
        this.prioritized = prioritized;
    }

    public int getReceiveTime() {
        return receiveTime;
    }

    public void setReceiveTime(int receiveTime) {
        this.receiveTime = receiveTime;
    }

    public int getAwaitStart() {
        return awaitStart;
    }

    public void setAwaitStart(int awaitStart) {
        this.awaitStart = awaitStart;
    }

    public int getAwaitEnd() {
        return awaitEnd;
    }

    public void setAwaitEnd(int awaitEnd) {
        this.awaitEnd = awaitEnd;
    }

    public int getProcessStart() {
        return processStart;
    }

    public void setProcessStart(int processStart) {
        this.processStart = processStart;
    }

    public int getProcessEnd() {
        return processEnd;
    }

    public void setProcessEnd(int processEnd) {
        this.processEnd = processEnd;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public int getProcessTime() {
        return processTime;
    }

    public void setProcessTime(int processTime) {
        this.processTime = processTime;
    }
}
