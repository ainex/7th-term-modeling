package edu.nstu.ulyanova.rgr;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class OrderHandler {
    private final int channelAmount;
    private Randomizer randomizer = Randomizer.getInstance();
    private LinkedList<Order> orders = new LinkedList<>();
    private List<Channel> channels = new ArrayList<>();
    private int incomingChannelTime = 0;
    private int processedOrdersAmount = 0;
    private Random random = new Random();

    public OrderHandler(int channelAmount) {
        for (int i = 0; i < channelAmount; i++) {
            channels.add(new Channel());
        }
        this.channelAmount = channelAmount;
    }

    public StatisticsCollector computeEverything(int simulationTime) {
        for (int i = 0; i < simulationTime; i++) {
            Order order = new Order();
            orders.add(order);
            order.setProcessTime(getProcessingTime());

            int timeBetweenOrders = getTimeBetweenOrders();
            incomingChannelTime += timeBetweenOrders;
            if (incomingChannelTime > simulationTime) {
                processedOrdersAmount = orders.indexOf(order);
                return getStatistic();
            }
            order.setReceiveTime(incomingChannelTime);
            channels.get(random.nextInt(channelAmount)).process(order, incomingChannelTime);

            i += timeBetweenOrders;
        }
//        for (Order order : orders) {
//            int timeBetweenOrders = getTimeBetweenOrders();
//            incomingChannelTime += timeBetweenOrders;
//            if (incomingChannelTime > simulationTime) {
//                processedOrdersAmount = orders.indexOf(order);
//                return getStatistic();
//            }
//            order.setReceiveTime(incomingChannelTime);
//            channels.get(random.nextInt(channelAmount)).process(order, incomingChannelTime);
//        }

        processedOrdersAmount = orders.size();
        return getStatistic();
    }

    private StatisticsCollector getStatistic() {
        StatisticsCollector statisticsCollector = new StatisticsCollector(channelAmount);
        statisticsCollector.setProcessedOrdersAmount(processedOrdersAmount);
        statisticsCollector.setTotalOrdersAmount(orders.size());
        statisticsCollector.setTotalProcessTime(channels.stream().mapToInt(Channel::getTotalProcessTime).sum());
        statisticsCollector.setTotalQueueLength(channels.stream().mapToInt(Channel::getTotalQueueSize).sum());
        statisticsCollector.setTotalQueueTime(channels.stream().mapToInt(Channel::getQueueTime).sum());
        return statisticsCollector;
    }

    private int getTimeBetweenOrders() {
        return randomizer.nextOrderInterval();
    }

    private int getProcessingTime() {
        return randomizer.nextProcessInterval();
    }
}
