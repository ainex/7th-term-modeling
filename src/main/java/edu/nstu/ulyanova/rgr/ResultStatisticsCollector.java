package edu.nstu.ulyanova.rgr;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collector;

/**
 * Результирующий класс для статистики по результатам N реализаций (прогонов)
 */
public class ResultStatisticsCollector {

    private static final String RESULT_FILE_NAME = "result.csv";
    private static final String DELIMITER = ";";
    private static final String FILE_HEADERS = "datetime" + DELIMITER + "averageQueueTime" + DELIMITER + " averageProcessTime" + DELIMITER + " averageQueueLength" + DELIMITER + " processedOrdersAmount" + DELIMITER + " averageChannelProcessTime" + DELIMITER + " processedProbability" + System.lineSeparator();


    private BigDecimal averageQueueTime = BigDecimal.ZERO;
    private BigDecimal averageProcessTime = BigDecimal.ZERO;
    private BigDecimal averageQueueLength = BigDecimal.ZERO;
    private BigDecimal processedOrdersAmount = BigDecimal.ZERO;
    private BigDecimal averageChannelProcessTime = BigDecimal.ZERO;
    private BigDecimal processedProbability = BigDecimal.ZERO;

    private ResultStatisticsCollector() {
    }

    /**
     * По данным N реализаций (прогонов) рассчитывает итоговые средние значения
     *
     * @param statisticsCollectors список полученных статистик в результате множества прогонов
     */
    public static ResultStatisticsCollector of(List<StatisticsCollector> statisticsCollectors) {
        int statisticsSize = statisticsCollectors.size();
        return statisticsCollectors.stream()
                .collect(
                        Collector.of(
                                ResultStatisticsCollector::new, //Supplier
                                ResultStatisticsCollector::reduce, //BiConsumer
                                ResultStatisticsCollector::combine, //Combiner
                                result -> ResultStatisticsCollector.terminal(result, statisticsSize)  //Finisher
                        )
                );
    }

    /*  Показатели эффективности функционирования объекта     */

    /**
     * @return среднее время простоя заявки в очереди
     */
    public BigDecimal getAverageQueueTime() {
        return averageQueueTime;
    }

    /**
     * @return среднее время обслуживания заявки
     */
    public BigDecimal getAverageProcessTime() {
        return averageProcessTime;
    }

    /**
     * @return средняя длина очереди
     */
    public BigDecimal getAverageQueueLength() {
        return averageQueueLength;
    }

    /**
     * @return количество обслуженных заявок за время моделирования
     */
    public BigDecimal getProcessedOrdersAmount() {
        return processedOrdersAmount;
    }

    /**
     * Сумма обслуживания всех зявок по всем каналам деленная на количество каналов
     *
     * @return среднее время работы канала обслуживания
     */
    public BigDecimal getAverageChannelProcessTime() {
        return averageChannelProcessTime;
    }

    /**
     * @return вероятность обслуживания заявки
     */
    public BigDecimal getProcessedProbability() {
        return processedProbability;
    }

    /**
     * Сворачивает поток результатов в один итоговый результат
     *
     * @param result тут пока промежуточный результат, сумма показателей из всех N прогонов
     * @param st     результаты каждого прогона
     */
    private static void reduce(ResultStatisticsCollector result, StatisticsCollector st) {
        result.averageQueueTime = result.averageQueueTime.add(st.getAverageQueueTime());
        result.averageProcessTime = result.averageProcessTime.add(st.getAverageProcessTime());
        result.averageQueueLength = result.averageQueueLength.add(st.getAverageQueueLength());
        result.processedOrdersAmount = result.processedOrdersAmount.add(BigDecimal.valueOf(st.getProcessedOrdersAmount()));
        result.averageChannelProcessTime = result.averageChannelProcessTime.add(st.getAverageChannelProcessTime());
        result.processedProbability = result.processedProbability.add(st.getProcessedProbability());
    }

    /**
     * @param result         промежуточный результат, каждый атрибут - это общая сумма, еще не среднее до деления на N
     * @param statisticsSize количество прогонов N, на которое поделить для получения среднего
     * @return итоговый средний результат с параметрами всех прогонов
     */
    private static ResultStatisticsCollector terminal(ResultStatisticsCollector result, int statisticsSize) {
        BigDecimal divider = BigDecimal.valueOf(statisticsSize);
        result.averageQueueTime = result.getAverageQueueTime().divide(divider, RoundingMode.HALF_UP);
        result.averageProcessTime = result.averageProcessTime.divide(divider, RoundingMode.HALF_UP);
        result.averageQueueLength = result.averageQueueLength.divide(divider, RoundingMode.HALF_UP);
        result.processedOrdersAmount = result.processedOrdersAmount.divide(divider, RoundingMode.HALF_UP);
        result.averageChannelProcessTime = result.averageChannelProcessTime.divide(divider, RoundingMode.HALF_UP);
        result.processedProbability = result.processedProbability.divide(divider, RoundingMode.HALF_UP);
        return result;
    }

    /**
     * Нужен только для паралельных стримов. Показывает, как объединять результаты. Все складывать.
     */
    private static ResultStatisticsCollector combine(ResultStatisticsCollector result1, ResultStatisticsCollector result2) {
        ResultStatisticsCollector combinedResult = new ResultStatisticsCollector();
        combinedResult.averageQueueTime = result1.averageQueueTime.add(result2.averageQueueTime);
        combinedResult.averageProcessTime = result1.averageProcessTime.add(result2.averageProcessTime);
        combinedResult.averageQueueLength = result1.averageQueueLength.add(result2.averageQueueLength);
        combinedResult.processedOrdersAmount = result1.processedOrdersAmount.add(result2.processedOrdersAmount);
        combinedResult.averageChannelProcessTime = result1.averageChannelProcessTime.add(result2.averageChannelProcessTime);
        combinedResult.processedProbability = result1.processedProbability.add(result2.processedProbability);
        return combinedResult;
    }

    public void saveToFile() {
        String contentToAppend = dataToLine();
        try {
            Path path = Paths.get(RESULT_FILE_NAME);
            StandardOpenOption option;

            if (path.toFile().exists()) {
                option = StandardOpenOption.APPEND;
            } else {
                contentToAppend = FILE_HEADERS + contentToAppend;
                option = StandardOpenOption.CREATE;
            }
            Files.write(Paths.get(RESULT_FILE_NAME), contentToAppend.getBytes(), option);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public String toString() {
        return new StringJoiner(", ", ResultStatisticsCollector.class.getSimpleName() + "[", "]")
                .add("averageQueueTime=" + averageQueueTime)
                .add("averageProcessTime=" + averageProcessTime)
                .add("averageQueueLength=" + averageQueueLength)
                .add("processedOrdersAmount=" + processedOrdersAmount)
                .add("averageChannelProcessTime=" + averageChannelProcessTime)
                .add("processedProbability=" + processedProbability)
                .toString();
    }

    private String dataToLine() {
        return LocalDateTime.now() + DELIMITER + averageQueueTime + DELIMITER +
                averageProcessTime + DELIMITER +
                averageQueueLength + DELIMITER +
                processedOrdersAmount + DELIMITER +
                averageChannelProcessTime + DELIMITER +
                processedProbability + System.lineSeparator();
    }
}
