package edu.nstu.ulyanova.rgr;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static edu.nstu.ulyanova.rgr.SimulationConstants.INITIAL_MODEL_RUNS_AMOUNT;
import static edu.nstu.ulyanova.rgr.SimulationConstants.T_ALPHA_OF_NORMAL_DISTRIBUTION;

public class Simulator {

    private final Params params;

    public Simulator(Params params) {
        this.params = params;

        Randomizer.setUp(params);
    }

    public void run() {
        List<StatisticsCollector> statistics = null;
        int optimalRunsAmount = INITIAL_MODEL_RUNS_AMOUNT;
        int runsAmount;

        if (params.fixedRuns) {
            //сделать фиксированное N количество прогонов, без учета статистической устойчивости
            statistics = IntStream.range(0, params.fixedRunsAmount)
                    .mapToObj(i -> doModeling()).collect(Collectors.toList());
        } else {
            do {
                runsAmount = optimalRunsAmount;
                //сделать N прогонов, обработать статистику и получить N*
                statistics = IntStream.range(0, runsAmount)
                        .mapToObj(i -> doModeling()).collect(Collectors.toList());

                optimalRunsAmount = getOptimalRunsAmount(statistics);

            } while (runsAmount < optimalRunsAmount);
        }

        // Собрали N* статистик, теперь посчитаем  средние по каждому показателю, т.к. прогонов было N*
        ResultStatisticsCollector resultStatistics = ResultStatisticsCollector.of(statistics);

        // Выводим результаты в консоль и в файл
        resultStatistics.saveToFile();
        System.out.println(resultStatistics.toString());
    }

    private int getOptimalRunsAmount(List<StatisticsCollector> statistics) {
        //получаем среднее выборки x̅
        double sampleMean = statistics.stream()
                .collect(Collectors.averagingDouble(s -> s.getAverageQueueTime().doubleValue()));

        //получаем σ2 – дисперсия оцениваемого параметра = ∑[(xi - x̅)^2]/n
        double squareSigma = statistics.stream()
                .collect(Collectors.averagingDouble(s -> Math.pow(s.getAverageQueueTime().doubleValue() - sampleMean, 2.0)));

        //количественный показатель точности, коэффициент эпсилон умножить на среднее значение
        double quantitativeEpsilon = params.keyIndicatorEpsilon * sampleMean;

        //оптимальное количество реализаций (прогонов) N*
        int optimalRunsAmount = (int) Math.round((squareSigma * T_ALPHA_OF_NORMAL_DISTRIBUTION * T_ALPHA_OF_NORMAL_DISTRIBUTION) / (quantitativeEpsilon * quantitativeEpsilon));
        return optimalRunsAmount;
    }

    //todo implement. Что вызвать, чтобы сделаеть один прогон и получить статистику?
    private StatisticsCollector doModeling() {
        OrderProducer orderProducer = new OrderProducer(params.channelsAmount);
        return orderProducer.getHandler().computeEverything(params.simulationTime);
    }
}
