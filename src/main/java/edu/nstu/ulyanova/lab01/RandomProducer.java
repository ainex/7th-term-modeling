package edu.nstu.ulyanova.lab01;

import java.util.*;
import java.util.stream.Collectors;

public class RandomProducer {
    private double upperBoundary = 4.0;
    private double lowerBoundary = 0.0;
    private double step = 0;
    private int N = 0;

    private void produce(int streamSize) {
        Random randomer = new Random();

        List<Double> doubles = randomer.doubles(streamSize)
                .map(this::map)
                .filter(this::checkBoundary)
                .boxed()
                .collect(Collectors.toList());
        System.out.println(doubles.size());
        HashMap<Double, Integer> zones = new LinkedHashMap<>();
        for (double i = 0; i < N; i++) {
            zones.put(i, 0);
        }
        for (Double aDouble : doubles) {
            int value = zones.get(Math.floor(aDouble / step));
            zones.put(Math.floor(aDouble / step), ++value);
        }
        System.out.println(zones);
        computeXSquared(zones, streamSize);
    }

    private boolean checkBoundary(double input) {
        return input >= lowerBoundary && input <= upperBoundary;
    }

    private double map(double input) {
        return (4 - 4 * Math.sqrt(1 - input));
    }

    private void computeXSquared(HashMap<Double, Integer> zones, int streamSize) {
        Set<Map.Entry<Double, Integer>> entries = zones.entrySet();
        double result = 0;
        for (Map.Entry<Double, Integer> entry : entries) {
            double integerValue = computeIntegerValue(entry.getKey());
            double X = (entry.getValue() - streamSize * integerValue * integerValue) / (streamSize * integerValue);
            result += X;
        }
        System.out.println("X^2 = " + result);
    }

    private double computeIntegerValue(double partNum) {
        double start = step * partNum;
        double end = step * (partNum + 1);
        return getFormulaValueOnX(end) - getFormulaValueOnX(start);
    }

    private double getFormulaValueOnX(double x) {
        return (0.5 * x) - ((0.5 * 0.5 * x * x) / 4);
    }

    private void setSectionsAmount(int N) {
        this.N = N;
        this.step = upperBoundary / N;
    }

    public static void main(String[] args) {
        RandomProducer orderProducer = new RandomProducer();
        orderProducer.setSectionsAmount(7);
        for (int i = 0; i < 100; i += 10) {
            orderProducer.produce(i == 0 ? 100 : i * 100);
        }
    }
}
