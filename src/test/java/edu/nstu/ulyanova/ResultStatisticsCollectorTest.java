package edu.nstu.ulyanova;

import edu.nstu.ulyanova.rgr.ResultStatisticsCollector;
import edu.nstu.ulyanova.rgr.StatisticsCollector;
import org.junit.Test;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ResultStatisticsCollectorTest {

    @Test
    public void shouldCollectResultsCorrectly() {
        ResultStatisticsCollector result = ResultStatisticsCollector.of(getStatistics());
        assertEquals(0, result.getAverageQueueTime().compareTo(BigDecimal.valueOf(250 / 5.0)));
        assertEquals(0, result.getAverageProcessTime().compareTo(BigDecimal.valueOf(100 / 5.0)));
        assertEquals(0, result.getAverageQueueLength().compareTo(BigDecimal.valueOf(20.0 / 5)));
        assertEquals(0, result.getProcessedOrdersAmount().compareTo(BigDecimal.valueOf(5)));
        assertEquals(0, result.getAverageChannelProcessTime().compareTo(BigDecimal.valueOf(100 / 2.0)));
        assertEquals(0, result.getProcessedProbability().compareTo(BigDecimal.valueOf(1)));
    }

    @Test
    public void shouldWriteToFile() throws IOException {
        Path path = Paths.get("result.csv");
        if (path.toFile().exists()) {
            Files.delete(path);
        }
        ResultStatisticsCollector result = ResultStatisticsCollector.of(getStatistics());
        result.saveToFile();

        assertTrue(path.toFile().exists());
    }

    private List<StatisticsCollector> getStatistics() {
        int totalQueueTime = 250;
        int totalProcessTime = 100;
        int totalQueueLength = 20;
        int processedOrdersAmount = 5;

        List<StatisticsCollector> statistics = IntStream.rangeClosed(1, 2).mapToObj(i -> {
            StatisticsCollector s = new StatisticsCollector(2);
            s.setTotalQueueTime(totalQueueTime);
            s.setTotalProcessTime(totalProcessTime);
            s.setTotalQueueLength(totalQueueLength);
            s.setProcessedOrdersAmount(processedOrdersAmount);
            s.setTotalOrdersAmount(processedOrdersAmount);
            return s;
        }).collect(Collectors.toList());

        return statistics;
    }
}
